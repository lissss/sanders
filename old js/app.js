'use strict';

/**
 * Created by lmaddock on 12/03/2018.
 * @ngdoc function
 * @name
 * @description
 *
 * Main module of the application.
 */
var app = angular.module('saundersApp', ['ngRoute']);
app.config(function ($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: 'index.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .when('/style', {
      templateUrl: 'index-headings.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .otherwise({
      redirectTo: '/'
    });


  // use the HTML5 history set
  //$locationProvider.html5Mode(true);
});



