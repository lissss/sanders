var sn = sn || {};
sn.components = sn.components || {};
sn.components.sidebar = function () {

  //default settings
  var settings = {
    autoCompleteAjaxUrl: '/data/suburbs.json',
    //autoCompleteAjaxUrl: 'http://staging.sanders.com.au/api/search/',
    sidebarTemplateUrl: '/partials/templates/sidebarMenuTemplate.hbs',
    sidebarDataUrl: '/data/sidebarMenu.json',
    sidebarContainer: '.sidebar-menu--container',
    sidebarWidth: '0px',
    desktopMinWidth: 1024,
    menuDesktopWidth: '600px',
    menuMobileWidth: '100%'
  }

  function initMenu() {
    loadTemplate();
    setupEventHandlers();
  }

  function loadTemplate() {
    $.ajax({
      type: 'GET',
      url: settings.sidebarDataUrl,
      dataType: 'json'
    })
      .done(function (data) {

        $.get(settings.sidebarTemplateUrl, function (source) {
          //load template and pass the source into the Handlebars compiler

          var template = Handlebars.compile(source);

          // pass the data in to render the template
          var wrapper = {objects: data}
          var theCompiledHtml = template(wrapper);

          //Insert the compiled html to the page
          $(settings.sidebarContainer).html(theCompiledHtml);

          //callback function
          callbackSideMenu();
        })
      })
      .fail(function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      })
  }

  function callbackSideMenu() {
    //init sidebar search dd
    var ddSearchType = new DropDown($('#ddSearchType'));
    var ddSearchProperty = new DropDown($('#ddSearchProperty'));
    var ddSearchBeds = new DropDown($('#ddSearchBeds'));
    var ddSearchPriceFrom = new DropDown($('#ddSearchPriceFrom'));
    var ddSearchPriceTo = new DropDown($('#ddSearchPriceTo'));
    var ddEnquiryType = new DropDown($('#ddEnquiryType'));

    //autocomplete options
    var options = {
      url: settings.autoCompleteAjaxUrl,
      getValue: 'address',
      list: {
        match: {
          enabled: true
        }
      },
      theme: 'square'
      /*ajaxSettings: {
       dataType: 'json',
       method: 'POST',
       data: {
       dataType: 'json'
       }
       }*/
    };

    //autocomplete init
    $('#searchField').easyAutocomplete(options);
    $('div.easy-autocomplete').removeAttr('style');

    var frm = $('#searchForm');
    frm.on('submit', function (event) {
      event.preventDefault();

      //TODO call api
      /* $.ajax({
       type: 'GET  ',
       url: settings.autoCompleteAjaxUrl,
       dataType: 'jsonp',
       jsonpCallback: 'logResults',
       data: frm.serialize()
       })
       .done(function (data) {
       console.log('Submission was successful.');
       console.log(data);
       })
       .fail(function (xhr, textStatus, error) {
       console.log(xhr.statusText);
       console.log(textStatus);
       console.log(error);
       })*/
    });
  }

  function setupEventHandlers() {
    $('.sidebar-search-form').hide();
    $('.sidebar-menu').hide();

    $(document).off('click', '.sidebar--toggle').on('click', '.sidebar--toggle', function (e) {
      //console.log('open menu');
      animateSideMenu(this, 'open', 'menu');
    });

    $(document).off('click', '.sidebar--search').on('click', '.sidebar--search', function (e) {
      //console.log('open search');
      animateSideMenu(this, 'open', 'search');
    });
    $(document).off('click', '.sidebar-menu--close').on('click', '.sidebar-menu--close', function () {
      //console.log('close');
      animateSideMenu(this, 'close');
    });
  }

  function animateSideMenu(target, state, contentType) {
    var sidebar = $('.sidebar');
    var sidebarOverlay = $('.sidebar-overlay');
    var sidebarToggle = $('.sidebar--toggle');
    var sidebarSLogo = $('.sidebar .s-logo');
    var sidebarSearch = $('.sidebar--search');
    var sidebarSearchForm = $('.sidebar-search-form');
    var menuContainer = $('.sidebar-menu--container');
    var menu = $('.sidebar-menu');
    var searchForm = $('.search-form');
    var menuFooter = $('.sidebar-menu--footer');
    var mainWrap = $('.main-wrap');
    var closeIcon = $('.sidebar-menu--close');
    var menuWidth = settings.menuMobileWidth;
    //menuContainer.on('transitionend webkitTransitionEnd oTransitionEnd', function () {//menuContainer.off();//});

    var sidebarOpen = [];
    var menuOpenAnimations = [];
    var searchOpenAnimations = [];
    var sidebarClose = [];
    var menuCloseAnimations = [];
    var searchCloseAnimations = [];
    var tempObj = {};

    if ($(window).width() > settings.desktopMinWidth) {
      menuWidth = settings.menuDesktopWidth;
    } else {
      menuWidth = settings.menuMobileWidth;
    }

    //Open Menu
    sidebarOpen = [{
      //fade in overlay
      e: sidebarOverlay,
      p: 'transition.fadeIn',
      o: {}
    }];

    //set menu width and display depending on screen size
    if ($(window).width() > settings.desktopMinWidth) {
      tempObj = {
        e: menuContainer,
        p: {
          width: menuWidth,
          opacity: 1,
        },
        o: {
          sequenceQueue: false,
          display: 'block'
        }
      }

    } else {
      tempObj = {
        e: menuContainer,
        p: 'transition.fadeIn',
        o: {
          duration: 200
        }
      }
    }
    //push to animation array
    sidebarOpen.push(tempObj);

    // menu open animations
    menuOpenAnimations = [{
      e: menu,
      p: {
        opacity: 1
      },
      o: {
        display: 'block'
      }
    }, {
      e: $('.sidebar-menu--container .site-logo'),
      p: {opacity: 1},
      o: {
        duration: 400,
        delay: 200
      }
    }, {
      e: $('.sidebar-menu--container .sidebar-menu--links-primary li'),
      p: 'transition.slideUpIn',
      o: {

        duration: 200,
        easing: 'easeOutCirc',
        stagger: 200,
      }
    }, {
      e: $('.sidebar-menu--container .sidebar-menu--links-secondary li'),
      p: 'transition.slideUpIn',
      o: {
        duration: 200,
        easing: 'easeOutCirc',
        stagger: 200
      }
    }, {
      e: $('.sidebar-menu--social'),
      p: 'transition.slideUpIn',
      o: {
        duration: 200,
        easing: 'easeOutCirc',
      }
    }, {
      e: $('.sidebar-menu--footer-links'),
      p: {
        opacity: 1
      },
      o: {
        complete: function () {
          //console.log('animation complete')
          menuFooter.css('position', 'fixed');
          menuContainer.css('overflow', 'auto');
          if ($(window).width() < settings.desktopMinWidth) {
            mainWrap.css('position', 'fixed');
          }
        }
      }
    }];

    //search open animations
    searchOpenAnimations = [{
      e: sidebarSearchForm,
      p: {
        opacity: 1
      },
      o: {
        display: 'block'
      }
    }, {
      e: $('.sidebar-menu--container .site-logo'),
      p: {opacity: 1},
      o: {
        duration: 400
      }
    }, {
      e: $('.sidebar-search-form--title'),
      p: 'transition.slideUpIn',
      o: {
        duration: 200,
        stagger: 200,
        easing: 'easeOutCirc'
      }
    }, {
      e: searchForm,
      p: {
        opacity: 1
      },
      o: {
        display: 'block'
      }
    }, {
      e: $('.sidebar-menu--footer-links'),
      p: {
        opacity: 1
      },
      o: {
        complete: function () {
          //console.log('animation complete');
          menuFooter.css('position', 'fixed');
          menuContainer.css('overflow', 'auto');
          if ($(window).width() < settings.desktopMinWidth) {
            mainWrap.css('position', 'fixed');
          }
        }
      }
    }];


    //Close Menu
    sidebarClose = [{
      //fade out overlay
      e: sidebarOverlay,
      p: 'transition.fadeOut',
      o: {}
    }, {
      e: menu,
      p: {
        opacity: 0
      },
      o: {
        display: 'none'
      }
    }, {
      e: searchForm,
      p: {
        opacity: 0
      },
      o: {
        display: 'none',
        sequenceQueue: false
      }
    }, {
      e: $('.sidebar-menu--footer-links'),
      p: {
        opacity: 0
      },
      o: {
        sequenceQueue: false
      }
    }, {
      e: $('.sidebar-menu--container .site-logo'),
      p: {opacity: 0},
      o: {
        sequenceQueue: false
      }
    }];

    //set menu hide type depending on screen size
    if ($(window).width() > settings.desktopMinWidth) {
      tempObj = {
        e: menuContainer,
        p: {
          width: settings.sidebarWidth,
        },
        o: {
          sequenceQueue: false
        }
      }
    } else {
      tempObj = {
        e: menuContainer,
        p: 'transition.fadeOut',
        o: {
          duration: 400
        }
      }
    }
    //push to animation array
    sidebarClose.push(tempObj);

    menuCloseAnimations = [
      {
        e: $('.sidebar-menu--container .sidebar-menu--links-primary li'),
        p: {opacity: 0}
      }, {
        e: $('.sidebar-menu--container .sidebar-menu--links-secondary li'),
        p: {opacity: 0}
      }, {
        e: $('.sidebar-menu--social'),
        p: {opacity: 0},
        o: {
          complete: function () {
            //console.log('animation complete')
            menuFooter.css('position', 'absolute');
            menuContainer.css('overflow', 'hidden');
            if ($(window).width() > settings.desktopMinWidth) {
              sidebar.removeClass('open');
            }
          }
        }
      }
    ];

    searchCloseAnimations = [{
      e: $('.sidebar-search-form--title'),
      p: {opacity: 0},
    }, {
      e: $('.search-form'),
      p: {opacity: 0},
      o: {
        display: 'none',
        complete: function () {
          //console.log('animation complete');
          menuFooter.css('position', 'absolute');
          menuContainer.css('overflow', 'hidden');
          if ($(window).width() > settings.desktopMinWidth) {
            sidebar.removeClass('open');
          }
        }
      }
    }];


    //Open Menu
    if (state === 'open') {
      //add classes
      sidebar.addClass(contentType);
      sidebar.addClass('open');
      menuContainer.addClass('open');
      sidebarToggle.addClass('active');
      sidebarToggle.prop('disabled', true);

      //push to animation array
      if (contentType === 'menu') {
        sidebarOpen.push.apply(sidebarOpen, menuOpenAnimations);
      }
      else {
        sidebarSearch.prop('disabled', true);
        sidebarOpen.push.apply(sidebarOpen, searchOpenAnimations);
      }
      //Run animations
      $.Velocity.RunSequence(sidebarOpen);

      //Close Menu
    } else {
      //remove classes
      menuContainer.removeClass('open');
      sidebarToggle.removeClass('active');
      sidebarToggle.prop('disabled', false);
      if ($(window).width() <= settings.desktopMinWidth) {
        sidebar.removeClass('open');
      }
      mainWrap.css('position', 'static');
      menuContainer.css('overflow', 'hidden');


      //push to animation array
      if (sidebar.hasClass('menu')) {
        sidebar.removeClass('menu');
        sidebarClose.push.apply(sidebarClose, menuCloseAnimations);
      }
      else {
        sidebar.removeClass('search');
        sidebarSearch.prop('disabled', false);
        sidebarClose.push.apply(sidebarClose, searchCloseAnimations);
      }

      //Run animations
      $.Velocity.RunSequence(sidebarClose);
    }
  }

  /**
   * Expose "public" methods
   */
  return {
    initMenu: initMenu
  };
}
;
$(document).ready(function () {
//init sidebar menu
  var sideMenu = sn.components.sidebar();
  sideMenu.initMenu();
});
