var sn = sn || {};
sn.components = sn.components || {};
sn.components.forms = function (options) {

  //default settings
  var settings = {
    messageStr: '',
    formSubmitUrl: 'http://localhost:9000/buy.html'
  };

  function initForm() {
    settings = $.extend({}, settings, options);

    // Add the novalidate attribute when the JS loads
    if (settings.novalidate) {
      var forms = document.querySelectorAll('.validate');
      for (var i = 0; i < forms.length; i++) {
        forms[i].setAttribute('novalidate', true);
      }
    }
    setupEventHandlers();
  }

  function setupEventHandlers() {
    // Listen to all blur events
    $('.form-control').on('blur', function (event) {
      if (event.target.value.length > 0) {
        event.target.classList.add('hasValue');
      } else {
        event.target.classList.remove('hasValue');
      }
    });

    $('.form-control-autosize').on('keyup', function (event) {
      var theDiv = $(this);
      var theDivContent = $(event.target).text();
      var theDivTextarea = theDiv.parent('.form-group').find('textarea');
      if (theDivContent.length > 0) {
        event.target.classList.add('hasValue');
        theDivTextarea.text(theDivContent);
      } else {
        event.target.classList.remove('hasValue');
        theDivTextarea.text('');
        theDiv.css('height', 'auto');
      }
    });

    //File input show file name labels on selection (for maintenance form)
    $('.file-uploader').each(function () {
      var fileInput = $(this);
      var fileLink = fileInput.prev('a');
      var fileLabel = fileInput.next('label');

      fileLink.on('click', function (event) {
        event.preventDefault();
        fileInput.trigger('click');
      });

      fileInput.on('change', function (event) {
        var fileName = event.target.value;
        displayFileName(fileLabel, fileName, this);
      });

      fileInput
        .on('focus', function () {
          fileInput.addClass('has-focus');
        })
        .on('blur', function () {
          fileInput.removeClass('has-focus');
        });
    });

    // Check all fields on submit
    $('form').on('submit', function (event) {
      event.preventDefault();
      var theForm = event.target;
      // Only run on forms flagged for validation
      if (!theForm.classList.contains('validate')) return;

      // Get all of the form elements
      var fields = theForm.elements;

      //Reset message errors
      settings.messageStr = '';

      // Validate each field
      var error, hasErrors;
      for (var i = 0; i < fields.length; i++) {
        error = validateField(fields[i]);
        if (error) {
          if (!hasErrors) {
            hasErrors = fields[i];
          }
        }
      }
      // Display error messages and focus on first element with error
      if (hasErrors) {
        displayErrors();
        event.preventDefault();
        hasErrors.focus();
      } else {
        //No errors - submit form
        //TODO change this to post to correct url
        $.ajax({
          type: 'GET',
          url: settings.formSubmitUrl,
          data: $(theForm).serialize()
        })
        .done(function (data) {
          console.log('Submission was successful.');
          $('.form-wrapper').remove();
          $('.submit-message').show();
        })
        .fail(function(xhr, textStatus, error){
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        })
      }

    });
  }

  // Validate the field
  var validateField = function (field) {

    // Don't validate submits, buttons, file and reset inputs, and disabled fields
    if (field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') return;

    // Get validity
    var validity = field.validity;

    // If valid, return null
    if (validity.valid) {
      removeError(field);
      return;
    //Otherwise set error status and message
    } else {
      addError(field);
    }
    return field;

  };

  //Set error status for field
  var addError = function (field) {
    // Add error class to field
    field.classList.add('error');
    field.closest('.form-group').classList.add('error');

    // Add ARIA role to the field
    field.setAttribute('aria-describedby', 'error-for-' + field.id);

    // Update error message
    if (field.getAttribute('title'))
      settings.messageStr += field.getAttribute('title') + '<br>';

  };

  // Remove the error status for field
  var removeError = function (field) {
    // Remove error class to field
    field.classList.remove('error');
    field.closest('.form-group').classList.remove('error');

    // Remove ARIA role from the field
    field.removeAttribute('aria-describedby');

  };

// Display the error messages
  var displayErrors = function () {
    if (settings.errorWrap) {
      $(settings.errorWrap).empty().html(settings.messageStr).show();
    } else {
      $('form').prepend('<div class="error-message">' + settings.messageStr + '</div>')
    }
  };

  var displayFileName = function(fileLabel, fileName, fileObj) {
    var fileName = '';
    var labelVal = fileLabel.html();

    if (fileObj.files && fileObj.files.length > 1) {
      fileName = ( this.getAttribute('data-multiple-caption') || '' ).replace('{count}', fileObj.files.length);
    }
    else if (event.target.value) {
      fileName = event.target.value.split('\\').pop();
    }
    if (fileName) {
      fileLabel.find('span').html(fileName);
      fileLabel.css('display','block');
    }
    else {
      fileLabel.html(labelVal);
      fileLabel.css('display','block');
    }
  };

  /**
   * Expose "public" methods
   */
  return {
    initForm: initForm
  };
};


//init forms
var formObj = sn.components.forms({
  'novalidate': true,
  'errorWrap': '.error-message'
});
formObj.initForm();
