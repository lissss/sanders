var sn = sn || {};
sn.components = sn.components || {};
sn.components.listing = function () {


  //default settings
  var settings = {
    propertyListTemplateUrl: '/partials/templates/listingTemplate.hbs',
    propertyListDataUrl: '/data/listing.json',
    propertyListContainer: '.listing-box--grid'
  }

  function initListing() {

    //Populate slider listings
    if ($('.property-listing--slider').length > 0) {
      loadTemplate(settings.propertyListDataUrl, settings.propertyListTemplateUrl, settings.propertyListContainer);
      //init sort dropdown
      settings.ddPropertySort = new DropDown($('#ddPropertySort'));
      setupEventHandlers();
    }

    //Property sale and rent page
    if (document.body.classList.contains('page-property-sale') || document.body.classList.contains('page-property-rent')) {
      setupPropertyPageSlider();
    }

    if (document.body.classList.contains('page-results')) {
      settings.ddResultsSort = new DropDown($('#ddSortBy'));
      setupSearchResults();
    }
  }

  //Loads data and handlebars templates
  function loadTemplate(dataUrl, templateUrl, target, data) {
    $.ajax({
      type: 'GET',
      url: dataUrl,
      data: data,
      dataType: 'json'
    })
      .done(function (data) {

        $.get(templateUrl, function (source) {
          //load template and pass the source into the Handlebars compiler

          var template = Handlebars.compile(source);

          // pass the data in to render the template
          var wrapper = {objects: data}
          var theCompiledHtml = template(wrapper);

          //Insert the compiled html to the page
          $(target).hide().html(theCompiledHtml).fadeIn(500);

          //callback function
          setupPropertySlider();
        })
      })
      .fail(function (xhr, textStatus, error) {
        console.log(xhr.statusText);
        console.log(textStatus);
        console.log(error);
      })
  }

  function setupEventHandlers() {
    // Property slider links
    var subNavLinks = $('.page-sub-navigation a');
    var subNavDropdownInput = settings.ddPropertySort.dd.find('.custom-selectbox-input');
    var btnViewAll = $('.property-listing--slider .btn-all');

    //click nav links
    subNavLinks.on('click', function (event) {
      event.preventDefault();
      var curr = $(event.target);

      if (!curr.hasClass('is-disabled')) {
        //set selected class
        subNavLinks.each(function (index, el) {
          $(el).removeClass('is-selected');
        });
        curr.addClass('is-selected');

        //update mobile dropdown values
        var ddSortList = settings.ddPropertySort.dd.find('.selectbox li');
        ddSortList.each(function (index, el) {
          var opt = $(el);
          if (curr.data('listtype') === opt.data('option-val')) {
            settings.ddPropertySort.update(curr.data('listtype'), index, curr.text())
          }
        });

        //load new content
        var dataObj = {listingType: curr.data('listtype')};
        loadTemplate(settings.propertyListDataUrl, settings.propertyListTemplateUrl, settings.propertyListContainer, dataObj);
      }
    });

    //change dropdown
    subNavDropdownInput.on('change', function (event) {
      subNavLinks.each(function (index, el) {
        $(el).removeClass('is-selected');
      })
      subNavLinks.each(function (index, el) {
        var opt = $(el);
        if (!opt.hasClass('is-disabled')) {
          if (opt.data('listtype') === settings.ddPropertySort.getOptionValue()) {
            opt.addClass('is-selected');
            var dataObj = {listingType: opt.data('listtype')};
          }
        }
      });

      //load new content
      loadTemplate(settings.propertyListDataUrl, settings.propertyListTemplateUrl, settings.propertyListContainer, dataObj);
    });

    //click item image - redirect to page
    $(document).on('click', '.property-listing--slider .panel-bg-image, .property-listing--slider .listing-box--title a', function (event) {
      event.preventDefault();
      window.location.href = $('.page-sub-navigation .is-selected').attr('href');
      //TODO ajax call
    });

    //click view all - redirect to page
    btnViewAll.on('click', function (event) {
      event.preventDefault();
      window.location.href = $('.page-sub-navigation .is-selected').attr('href');
    });
  }

  function setupPropertySlider() {
    //Property Listing slider init
    var propertyCarousel = $('.js-slider-property-listing');
    if (propertyCarousel.hasClass('owl-loaded')) {
      //refresh slider for new content
      propertyCarousel.trigger('destroy.owl.carousel');
    }
    propertyCarousel.owlCarousel({
      loop: true,
      dots: false,
      navText: ['<a class="prev"></a>', '<a class="next"></a>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
          stagePadding: 20
        },
        480: {
          items: 2,
          nav: true
        },
        1000: {
          items: 2,
          nav: true
        }
      }
    });
    $('.js-slider-property-listing').trigger('refresh.owl.carousel');
  }

  function setupPropertyPageSlider() {
    var carousel = $('.js-slider-property-header');
    var carouselDots = $('.owl-dots');
    var carouselIcons = $('.page-header--carousel-icons');
    var videoContainer = $('.page-header--video');
    var planContainer = $('.page-header--plan');

    carousel.on('initialized.owl.carousel changed.owl.carousel', function () {
      //TODO: add play and floorplan icons
      /*console.log('obj', $('.owl-dots'));
      console.log('width', $('.owl-dots').width());
      console.log('pos', $('.owl-dots').position());
      var dotsPosition =  $('.owl-dots').position();
      carouselIcons.css('left', dotsPosition.left);*/
    })

    carousel.owlCarousel({
      loop: true,
      nav: true,
      dots: true,
      navText: ['<a class="prev"></a>', '<a class="next"></a>'],
      items: 1,

    });
    carouselIcons.clone().appendTo($('.owl-dots'));

    $('.page-header--carousel-icons .icon-video').on('click', function(event) {
      event.preventDefault();
      console.log('show video');
      videoContainer.addClass('active');
    });

    $('.page-header--carousel-icons .icon-plan').on('click', function() {
      event.preventDefault();
      console.log('show plan');
      planContainer.addClass('active');
    });

    $('.page-header--video-close').on('click', function() {
      videoContainer.removeClass('active');
    });

    $('.page-header--plan-close').on('click', function() {
      planContainer.removeClass('active');
    });
  }

  function setupSearchResults() {
    var sortDropdownInput = settings.ddResultsSort.dd.find('.custom-selectbox-input');
    sortDropdownInput.on('change', function (event) {
      //load new content
      var dataObj = {listingType: settings.ddResultsSort.getOptionValue()};
      loadTemplate(settings.propertyListDataUrl, settings.propertyListTemplateUrl, settings.propertyListContainer, dataObj);
    });
  }

    /**
     * Expose "public" methods
     */
    return {
      initListing: initListing
    };
  };

  $(document).ready(function () {
    var listObj = sn.components.listing();
    listObj.initListing();
  });

