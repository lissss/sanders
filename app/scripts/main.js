//dom ready
$(document).ready(function () {
  //console.log('loading');

  //Get body classes
  var bodyClasses = document.body.classList;

  //Home Page
  if (bodyClasses.contains('page-home')) {
    //populate hover menu
    loadTemplate('/data/homeMenu.json', '/partials/templates/hoverMenuTemplate.hbs', '.menu-placeholder');
    setupTestimonialSlider();
  }
  //Buy Page
  else if (bodyClasses.contains('page-buy') || bodyClasses.contains('page-sell') || bodyClasses.contains('page-rent')) {
    //populate hover menu
    loadTemplate('/data/buyMenu.json', '/partials/templates/hoverMenuTemplate.hbs', '.menu-placeholder');
  }
});

//Loads data and handlebars templates
function loadTemplate(dataUrl, templateUrl, target, callback) {
  $.ajax({
    type: 'GET',
    url: dataUrl,
    dataType: 'json'
  })
    .done(function (data) {

      $.get(templateUrl, function (source) {
        //load template and pass the source into the Handlebars compiler

        var template = Handlebars.compile(source);

        // pass the data in to render the template
        var wrapper = {objects: data}
        var theCompiledHtml = template(wrapper);

        //Insert the compiled html to the page
        $(target).html(theCompiledHtml);

        //callback function
        var callbackFn = new Function(callback);
        callbackFn();
      })
    })
    .fail(function(xhr, textStatus, error){
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    })
}

function setupTestimonialSlider() {
  //Home Testimonial slider init
  $('.js-slider-testimonials').owlCarousel({
    loop:true,
    nav:true,
    autoplay: true,
    autoplayTimeout: 5000,
    dots: false,
    video: true,
    navText: ['<a class="prev"></a>','<a class="next"></a>'],
    items:1
  });

}

//Custom Dropdown
function DropDown(el) {
  this.dd = el;
  this.placeholder = this.dd.children('span');
  this.opts = this.dd.find('ul.selectbox > li');
  this.selectInput = this.dd.find('.custom-selectbox-input');
  this.data = '';
  this.val = '';
  this.index = -1;
  this.initEvents();
}

DropDown.prototype = {
  initEvents: function () {
    var obj = this;

    obj.dd.on('click', function (event) {
      $(this).toggleClass('active');
      return false;
    });

    obj.opts.on('click', function () {
      var opt = $(this);
      obj.data = opt.data('option-val');
      obj.val = opt.text();
      obj.index = opt.index();
      obj.placeholder.text(obj.val);
      obj.selectInput.val(obj.val).trigger('change');
    });
  },
  update: function (newValue, newIndex, newText) {
    this.val = newValue;
    this.index = newIndex;
    this.selectInput.val(newValue);
    this.placeholder.text(newText);
  },
  getOptionValue: function () {
    return this.data;
  },
  getValue: function () {
    return this.val;
  },
  getIndex: function () {
    return this.index;
  }
};

//Google maps init function (called on script tag load)
function initMap() {
  console.log('Google maps init..');

  var coords, contactMap;
  coords = {lat: -34.016524, lng: 151.0619316};
  contactMap = new google.maps.Map(document.getElementById('contactMap'), {
    zoom: 15,
    center: coords,
    styles: [
      {
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#f5f5f5'
          }
        ]
      },
      {
        'elementType': 'labels.icon',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#616161'
          }
        ]
      },
      {
        'elementType': 'labels.text.stroke',
        'stylers': [
          {
            'color': '#f5f5f5'
          }
        ]
      },
      {
        'featureType': 'administrative.land_parcel',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#bdbdbd'
          }
        ]
      },
      {
        'featureType': 'poi',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#eeeeee'
          }
        ]
      },
      {
        'featureType': 'poi',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#757575'
          }
        ]
      },
      {
        'featureType': 'poi.park',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#e5e5e5'
          }
        ]
      },
      {
        'featureType': 'poi.park',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#9e9e9e'
          }
        ]
      },
      {
        'featureType': 'road',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#ffffff'
          }
        ]
      },
      {
        'featureType': 'road.arterial',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#757575'
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#dadada'
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#616161'
          }
        ]
      },
      {
        'featureType': 'road.local',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#9e9e9e'
          }
        ]
      },
      {
        'featureType': 'transit.line',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#e5e5e5'
          }
        ]
      },
      {
        'featureType': 'transit.station',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#eeeeee'
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#c9c9c9'
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#9e9e9e'
          }
        ]
      }]
  });

  //Set custom Google maps marker for mobile and regular screens
  var mapIcon;

  const mobileWidth = window.matchMedia('(max-width: 480px)');
  if (mobileWidth.matches) {
    mapIcon = new google.maps.MarkerImage('../images/icon-map-sanders.png', null, null, null, new google.maps.Size(50, 55));
  } else {
    mapIcon = new google.maps.MarkerImage('../images/icon-map-sanders.png', null, null, null, new google.maps.Size(70, 76));
  }

  var marker = new google.maps.Marker({
    position: coords,
    map: contactMap,
    icon: mapIcon
  });

  console.log('Google map created');
}



